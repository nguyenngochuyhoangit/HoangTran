﻿<?php

/**

 * The header for our theme

 * This is the template that displays all of the <head> section and everything up until <div id="content">

 *

 * Header- Option 1.Left Logo Right Navbar 

 *

 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials

 *

 * @package Fashion

 */

?>



<?php

    $top_bar_enable_disable = ot_get_option( 'top_bar_enable_disable' );

    if($top_bar_enable_disable == "on"){

?>  

    <div class="top-bar left-logo" id="topbar">

        <div class="container">

            <?php $top_bar_style = ot_get_option( 'top_bar_style' );

            if($top_bar_style == "left_content_right_menu"){

                ?>

                 <div class="col-md-6 topleft">

                    <?php 

                        $top_bar_right_content = ot_get_option( 'top_bar_right_content' ); 

                        echo($top_bar_right_content); 

                    ?> 

                </div>

                <div class="col-md-6 topright">

                    <?php 

                        wp_nav_menu( array( 

                            'theme_location' => 'top-nav' ,

                            'fallback_cb'    => '__return_false',

                            'menu_class'     => 'nav navbar-nav navbar-right'

                            )

                        );

                    ?>

                </div>

            <?php

            }



            else if($top_bar_style =="left_menu_right_content"){

            ?>

            <div class="col-md-6 topright">

                <?php

                    wp_nav_menu( array( 

                        'theme_location' => 'top-nav' ,

                         'fallback_cb'    => '__return_false',

                        'menu_class'     => 'nav navbar-nav navbar-left',

                        )

                    );

                ?>

            </div>

            <div class="col-md-6 text-right">

                <?php 

                    $top_bar_right_content = ot_get_option( 'top_bar_right_content' ); 

                    echo($top_bar_right_content);

                ?>   

            </div>

        <?php  } ?>

        </div>

    </div>

<?php }?>





<?php $header_enable = ot_get_option( 'header_enable' );

    if($header_enable == "on"){ 

?>

<header>
    <link rel="stylesheet" href="https://cdn.samsung.com/etc/designs/smg/global/templates/page.css" type="text/css">
	<?php 

            wp_nav_menu( array( 

                'theme_location'    => 'menu-1',

                'depth'             => 4,

                'container'         => 'div',

                'container_class'   => 'collapse navbar-collapse',

                'container_id'      => 'bs-example-navbar-collapse-1',

                'menu_class'        => 'nav navbar-nav navbar-right',

                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',

                'walker'            => new wp_bootstrap_navwalker())

            );

            ?>
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top main-navigation">
	<div class="cm-g-gnb gnb">
                <nav class="gb-gnb" role="navigation">
                    <!--googleoff: all-->
                    <input type="hidden" name="newShopCountryYn" id="newShopCountryYn">
                    <div class="gb-gnb__dimmed" style="display: none"></div>
                    <div class="gb-gnb__bar">
                        <div class="gb-gnb__bar-inner">
                            <a href="" class="s-svg gb-gnb__logo"><span class="blind">Samsung</span></a>
                            <button type="button" class="s-svg s-gnb-toggler"><span class="blind">Open the Menu</span></button>
                            <ul class="gb-gnb__main" role="tablist">
                                <li class="s-depth1-wrap" role="presentation">
                                    <a href="#" class="s-btn-toggler js-toggler js-depth1-link" data-link-info="false^^" role="tab" data-omni="mobile" aria-selected="false">THIẾT BỊ</a>
                                    <div class="gb-gnb__drop-desktop" style="display: none; transition-property: opacity; transition-duration: 1ms; transition-timing-function: ease-in-out; opacity: 0;">
                                        <div class="gb-gnb__drop-inner" role="tabpanel">
                                            <div class="s-depth2-col">
                                                <ul>
                                                    <li class="s-depth2-wrap">
                                                        <a href="mobile/index.html" class="s-depth2-link" data-omni="mobile:mobile">Thiết Bị Di Động<span class="s-svg s-arw-right"></span></a>
                                                        <div class="s-depth3-wrap">
                                                            <ul>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="mobile:mobile:smartphones">Điện Thoại 1</a>
                                                                </li>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="mobile:mobile:tablets">Điện Thoại 2</a>
                                                                </li>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="mobile:mobile:wearables">Điện Thoại 3</a>
                                                                </li>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="mobile:mobile:other phones">Điện Thoại 4</a>
                                                                </li>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="mobile:mobile:accessories">Điện Thoại 5</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="s-depth2-col">
                                                <ul>
                                                    <li class="s-depth2-wrap">
                                                        <a href="mobile/index.html" class="s-depth2-link" data-omni="mobile:mobile apps">ỨNG DỤNG DI ĐỘNG<span class="s-svg s-arw-right"></span></a>
                                                        <div class="s-depth3-wrap">
                                                            <ul>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="mobile:mobile apps:samsung pay">Samsung Pay</a>
                                                                </li>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="mobile:mobile apps:samsung dex">Samsung Dex</a>
                                                                </li>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="mobile:mobile apps:smartthings">SmartThings</a>
                                                                </li>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="mobile:mobile apps:samsung health">Samsung Health</a>
                                                                </li>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="mobile:mobile apps:switch to galaxy">Đổi Sang Galaxy</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="s-depth2-col s-col-featured" style="height: 268px;">
                                                <ul>
                                                    <li class="s-depth2-wrap">
                                                        <span class="s-depth2-link" data-omni="mobile:featured">NỔI BẬT</span>
                                                        <div class="s-depth3-wrap">
                                                            <ul>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="mobile:featured:galaxy s9 s9 plus">Galaxy S9 | S9+</a>
                                                                </li>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="mobile:featured:galaxy note8">Galaxy Note8</a>
                                                                </li>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="mobile:featured:galaxy a8 a8 plus">Galaxy A8 | A8+</a>
                                                                </li>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="mobile:featured:galaxy a6  a6 plus">Galaxy A6 | A6+</a>
                                                                </li>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="mobile:featured:galaxy j7 pro">Galaxy J7 Pro</a>
                                                                </li>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="mobile:featured:gear 360 2017">Gear 360 2017</a>
                                                                </li>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="mobile:featured:multi device experience">Trải nghiệm trên nhiều thiết bị<br></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <button type="button" title="Đóng Menu" class="s-svg s-ico-close"><span class="blind">Đóng Menu</span></button>
                                        </div>
                                    </div>
                                </li>
                                <li class="s-depth1-wrap" role="presentation">
                                    <a href="#" class="s-btn-toggler js-toggler js-depth1-link" data-link-info="false^^" role="tab" data-omni="tv and av" aria-selected="false">THIẾT BỊ KHÁC</a>
                                    <div class="gb-gnb__drop-desktop" style="transition-property: opacity; transition-duration: 100ms; transition-timing-function: ease-in-out; opacity: 0;">
                                        <div class="gb-gnb__drop-inner" role="tabpanel">
                                            <div class="s-depth2-col">
                                                <ul>
                                                    <li class="s-depth2-wrap">
                                                        <a href="mobile/index.html" class="s-depth2-link" data-omni="tv and av:tv">TV<span class="s-svg s-arw-right"></span></a>
                                                        <div class="s-depth3-wrap">
                                                            <ul>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="tv and av:tv:qled tv">QLED 4K TV</a>
                                                                </li>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="tv and av:tv:premium uhd">Premium UHD 4K TV</a>
                                                                </li>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="tv and av:tv:uhd tv">UHD 4K TV</a>
                                                                </li>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="tv and av:tv:full hd tv">Full HD TV</a>
                                                                </li>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="tv and av:tv:hd tv">HD TV</a>
                                                                </li>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="tv and av:tv:the frame">The Frame</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li class="s-depth2-wrap">
                                                        <span class="s-depth2-link" data-omni="tv and av:tv apps">ỨNG DỤNG TV</span>
                                                        <div class="s-depth3-wrap">
                                                            <ul>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="tv and av:tv apps:smart hub">Smart Hub</a>
                                                                </li>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="tv and av:tv apps:smart view">Smart View</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="s-depth2-col">
                                                <ul>
                                                    <li class="s-depth2-wrap">
                                                        <a href="mobile/index.html" class="s-depth2-link" data-omni="tv and av:audio video">THIẾT BỊ NGHE NHÌN<span class="s-svg s-arw-right"></span></a>
                                                        <div class="s-depth3-wrap">
                                                            <ul>
                                                                <li>
                                                                    <a href="aboutsamsung/sustainability/strategy/index.html" class="s-depth3-link" data-omni="tv and av:audio video:soundbar sound plus">Loa Thanh Sound+ Thế Hệ Mới</a>
                                                                </li>
                                                                <li>
                                                                    <a href="audio-video/soundbar/index.html" class="s-depth3-link" data-omni="tv and av:audio video:soundbar">Loa Thanh</a>
                                                                </li>
                                                                <li>
                                                                    <a href="audio-video/wireless-audio/index.html" class="s-depth3-link" data-omni="tv and av:audio video:wireless audio">Âm Thanh Không Dây Đa Chiều</a>
                                                                </li>
                                                                <li>
                                                                    <a href="audio-video/home-entertainment-system/index.html" class="s-depth3-link" data-omni="tv and av:audio video:home entertaiment">Dàn Giải Trí Tại Gia</a>
                                                                </li>
                                                                <li>
                                                                    <a href="audio-video/blu-ray-dvd-player/index.html" class="s-depth3-link" data-omni="tv and av:audio video:blu-ray and dvd player">Đầu Phát Blu-ray &amp; DVD</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="s-depth2-col s-col-featured">
                                                <ul>
                                                    <li class="s-depth2-wrap">
                                                        <span class="s-depth2-link" data-omni="tv and av:featured">NỔI BẬT</span>
                                                        <div class="s-depth3-wrap">
                                                            <ul>
                                                                <li>
                                                                    <a href="tvs/qled-q9f/QA75Q9FAMKXXV/index.html" class="s-depth3-link" data-omni="tv and av:featured:qled tv 4k q9f">QLED TV 4K Q9F</a>
                                                                </li>
                                                                <li>
                                                                    <a href="tvs/qled-q8c/QA65Q8CAMKXXV/index.html" class="s-depth3-link" data-omni="tv and av:featured:qled tv 4k q8c">QLED TV 4K Q8C </a>
                                                                </li>
                                                                <li>
                                                                    <a href="tvs/qled-q7f/QA65Q7FAMKXXV/index.html" class="s-depth3-link" data-omni="tv and av:featured:qled tv 4k q7f">QLED TV 4K Q7F </a>
                                                                </li>
                                                                <li>
                                                                    <a href="tvs/premium-uhd-mu9000/UA65MU9000KXXV/index.html" class="s-depth3-link" data-omni="tv and av:featured:premium uhd mu9000">Premium UHD MU9000</a>
                                                                </li>
                                                                <li>
                                                                    <a href="audio-video/soundbar-hw-ms6501/index.html" class="s-depth3-link" data-omni="tv and av:featured:loa thanh hw-ms6501">Loa Cong HW-MS6501</a>
                                                                </li>
                                                                <li>
                                                                    <a href="audio-video/soundbar-k950/index.html" class="s-depth3-link" data-omni="tv and av:featured:loa thanh hw-k950">Loa Thanh HW-K950</a>
                                                                </li>
                                                                <li>
                                                                    <a href="multi-device-experience/index.html" class="s-depth3-link" data-omni="tv and av:featured:multi device experience">Trải nghiệm trên nhiều thiết bị<br></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li class="s-depth2-wrap">
                                                        <span class="s-depth2-link" data-omni="tv and av:magazine">MAGAZINE</span>
                                                        <div class="s-depth3-wrap">
                                                            <ul>
                                                                <li>
                                                                    <a href="curation-contents/theframe/curated-collections/index.html" class="s-depth3-link" data-omni="tv and av:magazine:curated art with the frame">Bộ Sưu Tập The Frame - TV Khung Tranh</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <button type="button" title="Đóng Menu" class="s-svg s-ico-close"><span class="blind">Đóng Menu</span></button>
                                        </div>
                                    </div>
                                </li>
                                <li class="s-depth1-wrap" role="presentation">
                                    <a href="#" class="s-btn-toggler js-toggler js-depth1-link" data-link-info="false^^" role="tab" data-omni="appliances" aria-selected="false">PHỤ KIỆN</a>
                                    <div class="gb-gnb__drop-desktop" style="transition-property: opacity; transition-duration: 100ms; transition-timing-function: ease-in-out; opacity: 0;">
                                        <div class="gb-gnb__drop-inner" role="tabpanel">
                                            <div class="s-depth2-col">
                                                <ul>
                                                    <li class="s-depth2-wrap">
                                                        <a href="mobile/index.html" class="s-depth2-link" data-omni="appliances:home appliances">GIA DỤNG<span class="s-svg s-arw-right"></span></a>
                                                        <div class="s-depth3-wrap">
                                                            <ul>
                                                                <li>
                                                                    <a href="refrigerators/index.html" class="s-depth3-link" data-omni="appliances:home appliances:refrigerators">Tủ Lạnh</a>
                                                                </li>
                                                                <li>
                                                                    <a href="cooking-appliances/all-cooking-appliances/index.html" class="s-depth3-link" data-omni="appliances:home appliances:cooking appliances">Thiết Bị Nhà Bếp</a>
                                                                </li>
                                                                <li>
                                                                    <a href="washing-machines/index.html" class="s-depth3-link" data-omni="appliances:home appliances:washing machines">Máy Giặt</a>
                                                                </li>
                                                                <li>
                                                                    <a href="air-conditioners/index.html" class="s-depth3-link" data-omni="appliances:home appliances:air conditioners">Máy Điều Hòa</a>
                                                                </li>
                                                                <li>
                                                                    <a href="vacuum-cleaners/index.html" class="s-depth3-link" data-omni="appliances:home appliances:vacuum cleaners">Máy Hút Bụi</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="s-depth2-col">
                                            </div>
                                            <div class="s-depth2-col s-col-featured">
                                                <ul>
                                                    <li class="s-depth2-wrap">
                                                        <span class="s-depth2-link" data-omni="appliances:featured">NỔI BẬT</span>
                                                        <div class="s-depth3-wrap">
                                                            <ul>
                                                                <li>
                                                                    <a href="may-giat-add-wash/index.html" class="s-depth3-link" data-omni="appliances:featured:addwash">AddWash</a>
                                                                </li>
                                                                <li class="s-only-mobile">
                                                                    <a href="may-giat-activ-dual-wash/index.html" class="s-depth3-link" data-omni="appliances:featured:activ dualwash">Activ DualWash</a>
                                                                </li>
                                                                <li>
                                                                    <a href="washing-machines/flexwash-wr24m9960kvsv/index.html" class="s-depth3-link" data-omni="appliances:featured:flexwash">FlexWash</a>
                                                                </li>
                                                                <li>
                                                                    <a href="tu-lanh-twin-cooling-plus/index.html" class="s-depth3-link" data-omni="appliances:featured:twin cooling plus tmf">Twin Cooling Plus TMF</a>
                                                                </li>
                                                                <li>
                                                                    <a href="refrigerators/multi-door-rf56k9041sg/index.html" class="s-depth3-link" data-omni="appliances:featured:multidoor">Multidoor</a>
                                                                </li>
                                                                <li>
                                                                    <a href="refrigerators/side-by-side-rh58k6687sl/index.html" class="s-depth3-link" data-omni="appliances:featured:side by side">Side by Side</a>
                                                                </li>
                                                                <li>
                                                                    <a href="multi-device-experience/index.html" class="s-depth3-link" data-omni="appliances:featured:multi device experience">Trải nghiệm trên nhiều thiết bị<br></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="s-depth2-col s-col-imagepromo">
                                                <a href="home-appliances/index.html" data-omni="AddWash"><img src="//images.samsung.com/is/image/samsung/p5/vn/home/navigation-images-gnb-appliances.jpg?$ORIGIN_JPG$" width="425" height="240" alt="AddWash"></a>
                                            </div>
                                            <button type="button" title="Đóng Menu" class="s-svg s-ico-close"><span class="blind">Đóng Menu</span></button>
                                        </div>
                                    </div>
                                </li>
                                <li class="s-depth1-wrap" role="presentation">
                                    <a href="#" class="s-btn-toggler js-toggler js-depth1-link" data-link-info="false^^" role="tab" data-omni="appliances" aria-selected="false">GIẢI PHÁP</a>
                                    <div class="gb-gnb__drop-desktop" style="transition-property: opacity; transition-duration: 100ms; transition-timing-function: ease-in-out; opacity: 0;">
                                    </div>
                                </li>
                            </ul>
                            <div class="gb-gnb__drop-mobile drilldown" style="min-height: 575px">
                                <div class="gb-gnb__drop-mobile-wrap drilldown-container">
                                    <div class="drilldown-root">
                                        <ul class="s-depth1-wrap" id="mobile_left">
                                            <li>
                                                <a href="#" class="s-depth1-link" data-omni="mobile">DI ĐỘNG<span class="s-svg s-arw-right"></span></a>
                                                <div class="drilldown-sub">
                                                    <div class="drilldown-back">
                                                        <a href="#" class="s-btn-back"><span class="s-svg s-arw-left"></span><span class="blind">Quay lại Menu Chiều sâu 1</span></a>
                                                    </div>
                                                    <ul>
                                                        <li class="s-depth2-wrap">
                                                            <a href="mobile/index.html" class="s-depth2-link" data-omni="mobile:mobile">Thiết Bị Di Động<span class="s-svg s-arw-right"></span></a>
                                                            <ul>
                                                                <li>
                                                                    <a class="s-depth3-link" href="smartphones/index.html" data-omni="mobile:mobile:smartphones">Điện Thoại Thông Minh</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="tablets/index.html" data-omni="mobile:mobile:tablets">Máy Tính Bảng</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="wearables/index.html" data-omni="mobile:mobile:wearables">Thiết Bị Đeo</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="mobile-phones/all-mobile-phones/index.html" data-omni="mobile:mobile:other phones">Điện Thoại Phổ Thông</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="mobile-accessories/index.html" data-omni="mobile:mobile:accessories">Phụ Kiện</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="s-depth2-wrap">
                                                            <a href="mobile/index.html" class="s-depth2-link" data-omni="mobile:mobile apps">ỨNG DỤNG DI ĐỘNG<span class="s-svg s-arw-right"></span></a>
                                                            <ul>
                                                                <li>
                                                                    <a class="s-depth3-link" href="samsungpay/index.html" data-omni="mobile:mobile apps:samsung pay">Samsung Pay</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="apps/samsung-dex/index.html" data-omni="mobile:mobile apps:samsung dex">Samsung Dex</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="apps/smartthings/index.html" data-omni="mobile:mobile apps:smartthings">SmartThings</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="apps/samsung-health/index.html" data-omni="mobile:mobile apps:samsung health">Samsung Health</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="campaign/switch-to-galaxy/index.html" data-omni="mobile:mobile apps:switch to galaxy">Đổi Sang Galaxy</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="s-depth2-wrap">
                                                            <span class="s-depth2-link" data-omni="mobile:featured">NỔI BẬT</span>
                                                            <ul>
                                                                <li>
                                                                    <a class="s-depth3-link" href="smartphones/galaxy-s9/index.html" data-omni="mobile:featured:galaxy s9 s9 plus">Galaxy S9 | S9+</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="smartphones/galaxy-note8/index.html" data-omni="mobile:featured:galaxy note8">Galaxy Note8</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="galaxy/a8/index.html" data-omni="mobile:featured:galaxy a8 a8 plus">Galaxy A8 | A8+</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="galaxy/a6/index.html" data-omni="mobile:featured:galaxy a6  a6 plus">Galaxy A6 | A6+</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="galaxy/j7pro/index.html" data-omni="mobile:featured:galaxy j7 pro">Galaxy J7 Pro</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="wearables/gear-360-2017-r210/index.html" data-omni="mobile:featured:gear 360 2017">Gear 360 2017</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="multi-device-experience/index.html" data-omni="mobile:featured:multi device experience">Trải nghiệm trên nhiều thiết bị<br></a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li>
                                                <a href="#" class="s-depth1-link" data-omni="tv and av">TV &amp; AV<span class="s-svg s-arw-right"></span></a>
                                                <div class="drilldown-sub">
                                                    <div class="drilldown-back">
                                                        <a href="#" class="s-btn-back"><span class="s-svg s-arw-left"></span><span class="blind">Quay lại Menu Chiều sâu 1</span></a>
                                                    </div>
                                                    <ul>
                                                        <li class="s-depth2-wrap">
                                                            <a href="mobile/index.html" class="s-depth2-link" data-omni="tv and av:tv">TV<span class="s-svg s-arw-right"></span></a>
                                                            <ul>
                                                                <li>
                                                                    <a class="s-depth3-link" href="tvs/qled/index.html" data-omni="tv and av:tv:qled tv">QLED 4K TV</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="tvs/premium-uhd/index.html" data-omni="tv and av:tv:premium uhd">Premium UHD 4K TV</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="tvs/uhd-tv/index.html" data-omni="tv and av:tv:uhd tv">UHD 4K TV</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="tvs/full-hd-tv/?full-hd-tv/index.html" data-omni="tv and av:tv:full hd tv">Full HD TV</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="tvs/hd-tv/?hd/index.html" data-omni="tv and av:tv:hd tv">HD TV</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="tvs/theframetv/highlights/index.html" data-omni="tv and av:tv:the frame">The Frame</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="s-depth2-wrap">
                                                            <span class="s-depth2-link" data-omni="tv and av:tv apps">ỨNG DỤNG TV</span>
                                                            <ul>
                                                                <li>
                                                                    <a class="s-depth3-link" href="//www.samsung.com/vn/smarthub/index.html" data-omni="tv and av:tv apps:smart hub">Smart Hub</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="apps/tv/smart-view/index.html" data-omni="tv and av:tv apps:smart view">Smart View</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="s-depth2-wrap">
                                                            <a href="mobile/index.html" class="s-depth2-link" data-omni="tv and av:audio video">THIẾT BỊ NGHE NHÌN<span class="s-svg s-arw-right"></span></a>
                                                            <ul>
                                                                <li>
                                                                    <a class="s-depth3-link" href="audio-video/soundbar-soundplus/index.html" data-omni="tv and av:audio video:soundbar sound plus">Loa Thanh Sound+ Thế Hệ Mới</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="audio-video/soundbar/index.html" data-omni="tv and av:audio video:soundbar">Loa Thanh</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="audio-video/wireless-audio/index.html" data-omni="tv and av:audio video:wireless audio">Âm Thanh Không Dây Đa Chiều</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="audio-video/home-entertainment-system/index.html" data-omni="tv and av:audio video:home entertaiment">Dàn Giải Trí Tại Gia</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="audio-video/blu-ray-dvd-player/index.html" data-omni="tv and av:audio video:blu-ray and dvd player">Đầu Phát Blu-ray &amp; DVD</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="s-depth2-wrap">
                                                            <span class="s-depth2-link" data-omni="tv and av:featured">NỔI BẬT</span>
                                                            <ul>
                                                                <li>
                                                                    <a class="s-depth3-link" href="tvs/qled-q9f/QA75Q9FAMKXXV/index.html" data-omni="tv and av:featured:qled tv 4k q9f">QLED TV 4K Q9F</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="tvs/qled-q8c/QA65Q8CAMKXXV/index.html" data-omni="tv and av:featured:qled tv 4k q8c">QLED TV 4K Q8C </a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="tvs/qled-q7f/QA65Q7FAMKXXV/" data-omni="tv and av:featured:qled tv 4k q7f">QLED TV 4K Q7F </a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="tvs/premium-uhd-mu9000/UA65MU9000KXXV/index.html" data-omni="tv and av:featured:premium uhd mu9000">Premium UHD MU9000</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="audio-video/soundbar-hw-ms6501/index.html" data-omni="tv and av:featured:loa thanh hw-ms6501">Loa Cong HW-MS6501</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="audio-video/soundbar-k950/index.html" data-omni="tv and av:featured:loa thanh hw-k950">Loa Thanh HW-K950</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="multi-device-experience/index.html" data-omni="tv and av:featured:multi device experience">Trải nghiệm trên nhiều thiết bị<br></a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="s-depth2-wrap">
                                                            <span class="s-depth2-link" data-omni="tv and av:magazine">MAGAZINE</span>
                                                            <ul>
                                                                <li>
                                                                    <a class="s-depth3-link" href="curation-contents/theframe/curated-collections/index.html" data-omni="tv and av:magazine:curated art with the frame">Bộ Sưu Tập The Frame - TV Khung Tranh</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li>
                                                <a href="#" class="s-depth1-link" data-omni="appliances">GIA DỤNG<span class="s-svg s-arw-right"></span></a>
                                                <div class="drilldown-sub">
                                                    <div class="drilldown-back">
                                                        <a href="#" class="s-btn-back"><span class="s-svg s-arw-left"></span><span class="blind">Quay lại Menu Chiều sâu 1</span></a>
                                                    </div>
                                                    <ul>
                                                        <li class="s-depth2-wrap">
                                                            <a href="mobile/index.html" class="s-depth2-link" data-omni="appliances:home appliances">GIA DỤNG<span class="s-svg s-arw-right"></span></a>
                                                            <ul>
                                                                <li>
                                                                    <a class="s-depth3-link" href="refrigerators/index.html" data-omni="appliances:home appliances:refrigerators">Tủ Lạnh</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="cooking-appliances/all-cooking-appliances/index.html" data-omni="appliances:home appliances:cooking appliances">Thiết Bị Nhà Bếp</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="washing-machines/index.html" data-omni="appliances:home appliances:washing machines">Máy Giặt</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="air-conditioners/index.html" data-omni="appliances:home appliances:air conditioners">Máy Điều Hòa</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="vacuum-cleaners/index.html" data-omni="appliances:home appliances:vacuum cleaners">Máy Hút Bụi</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="s-depth2-wrap">
                                                            <span class="s-depth2-link" data-omni="appliances:featured">NỔI BẬT</span>
                                                            <ul>
                                                                <li>
                                                                    <a class="s-depth3-link" href="may-giat-add-wash/index.html" data-omni="appliances:featured:addwash">AddWash</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="may-giat-activ-dual-wash/index.html" data-omni="appliances:featured:activ dualwash">Activ DualWash</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="washing-machines/flexwash-wr24m9960kvsv/index.html" data-omni="appliances:featured:flexwash">FlexWash</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="tu-lanh-twin-cooling-plus/index.html" data-omni="appliances:featured:twin cooling plus tmf">Twin Cooling Plus TMF</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="refrigerators/multi-door-rf56k9041sg/index.html" data-omni="appliances:featured:multidoor">Multidoor</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="refrigerators/side-by-side-rh58k6687sl/index.html" data-omni="appliances:featured:side by side">Side by Side</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="multi-device-experience/index.html" data-omni="appliances:featured:multi device experience">Trải nghiệm trên nhiều thiết bị<br></a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li>
                                                <a href="#" class="s-depth1-link" data-omni="more">THIẾT BỊ KHÁC<span class="s-svg s-arw-right"></span></a>
                                                <div class="drilldown-sub">
                                                    <div class="drilldown-back">
                                                        <a href="#" class="s-btn-back"><span class="s-svg s-arw-left"></span><span class="blind">Quay lại Menu Chiều sâu 1</span></a>
                                                    </div>
                                                    <ul>
                                                        <li class="s-depth2-wrap">
                                                            <span class="s-depth2-link" data-omni="more:it">IT</span>
                                                            <ul>
                                                                <li>
                                                                    <a class="s-depth3-link" href="monitors/index.html" data-omni="more:it:monitors">Màn Hình</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="s-depth2-wrap">
                                                            <span class="s-depth2-link" data-omni="more:featured">NỔI BẬT</span>
                                                            <ul>
                                                                <li>
                                                                    <a class="s-depth3-link" href="monitors/curved-cf591/index.html" data-omni="more:featured:curved">Màn Hình Cong</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="multi-device-experience/index.html" data-omni="more:featured:multi device experience">Trải nghiệm trên nhiều thiết bị<br></a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li>
                                                <a href="#" class="s-depth1-link" data-omni="shop">SHOP<span class="s-svg s-arw-right"></span></a>
                                                <div class="drilldown-sub">
                                                    <div class="drilldown-back">
                                                        <a href="#" class="s-btn-back"><span class="s-svg s-arw-left"></span><span class="blind">Quay lại Menu Chiều sâu 1</span></a>
                                                    </div>
                                                    <ul>
                                                        <li class="s-depth2-wrap">
                                                            <a href="mobile/index.html" class="s-depth2-link" data-omni="shop:mobile">Điện thoại thông minh<span class="s-svg s-arw-right"></span></a>
                                                            <ul>
                                                                <li>
                                                                    <a class="s-depth3-link" href="//shop.samsung.com/vn/c/MO0102" data-omni="shop:mobile:galaxy note">Galaxy Note</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="//shop.samsung.com/vn/c/MO0101" data-omni="shop:mobile:galaxy s">Galaxy S</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="//shop.samsung.com/vn/c/MO0103" data-omni="shop:mobile:galaxy a">Galaxy A</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="//shop.samsung.com/vn/c/MO0104" data-omni="shop:mobile:galaxy j">Galaxy J</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="//shop.samsung.com/vn/c/MO0105" data-omni="shop:mobile:galaxy c">Galaxy C</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="s-depth2-wrap">
                                                            <a href="mobile/index.html" class="s-depth2-link" data-omni="shop:null">Máy tính bảng<span class="s-svg s-arw-right"></span></a>
                                                            <ul>
                                                                <li>
                                                                    <a class="s-depth3-link" href="//shop.samsung.com/vn/c/MO0302" data-omni="shop:null:galaxy tab a">Galaxy Tab A</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="//shop.samsung.com/vn/c/MO0303" data-omni="shop:null:galaxy tab e">Galaxy Tab E</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="s-depth2-wrap">
                                                            <a href="mobile/index.html" class="s-depth2-link" data-omni="shop:wearables">Thiết bị đeo<span class="s-svg s-arw-right"></span></a>
                                                        </li>
                                                        <li class="s-depth2-wrap">
                                                            <a href="mobile/index.html" class="s-depth2-link" data-omni="shop:null">Phụ kiện di động<span class="s-svg s-arw-right"></span></a>
                                                            <ul>
                                                                <li>
                                                                    <a class="s-depth3-link" href="//shop.samsung.com/vn/c/MO0501" data-omni="shop:null:null">Ốp lưng / Bao da</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="//shop.samsung.com/vn/c/MO0502" data-omni="shop:null:null">Pin / sạc</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="//shop.samsung.com/vn/c/MO0503" data-omni="shop:null:null">Loa / Tai nghe</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="s-depth2-wrap">
                                                            <a href="mobile/index.html" class="s-depth2-link" data-omni="shop:tv &amp; av">TIVI<span class="s-svg s-arw-right"></span></a>
                                                            <ul>
                                                                <li>
                                                                    <a class="s-depth3-link" href="//shop.samsung.com/vn/c/AV0106" data-omni="shop:tv &amp; av:null">Tivi Khung tranh</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="s-depth2-wrap">
                                                            <a href="mobile/index.html" class="s-depth2-link" data-omni="shop:null">THIẾT BỊ NGHE NHÌN<span class="s-svg s-arw-right"></span></a>
                                                        </li>
                                                        <li class="s-depth2-wrap">
                                                            <a href="smobile/index.html" class="s-depth2-link" data-omni="shop:electronics domestic">Thiết bị gia dụng<span class="s-svg s-arw-right"></span></a>
                                                            <ul>
                                                                <li>
                                                                    <a class="s-depth3-link" href="//shop.samsung.com/vn/c/DA02" data-omni="shop:electronics domestic:null">Lò Vi Sóng</a>
                                                                </li>
                                                                <li>
                                                                    <a class="s-depth3-link" href="//shop.samsung.com/vn/c/DA03" data-omni="shop:electronics domestic:null">Máy Hút Bụi</a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                        <!-- 20180305 한총 모바일 GNB 변경 :: S -->
                                        <!-- 20180305 한총 모바일 GNB 변경 :: E -->
                                        <ul class="s-depth1-wrap" id="mobile_right">
                                            <li>
                                                <a class="s-depth1-link" href="//www.samsung.com/vn/support/" data-omni="support">HỖ TRỢ</a>
                                                <div class="custom-layer-content" style="display:none">
                                                </div>
                                            </li>
                                            <li>
                                                <a class="s-depth1-link" href="//www.samsung.com/vn/business" target="_blank" data-omni="business" title="DOANH NGHIỆP : Mở trong một Cửa sổ Mới">DOANH NGHIỆP<span class="s-svg s-ico-new-window"></span></a>
                                                <div class="custom-layer-content" style="display:none">
                                                </div>
                                            </li>
                                            <!-- 20170608 한총 신규 마크업 분기처리 :: S -->
                                            <!-- 20180430 SENA 국가 Login 마크업 수정(MOBILE) :: E -->
                                            <li>
                                                <a href="https://account.samsung.com/mobile/account/check.do" class="s-login s-signin s-ico-login" data-omni="login">Đăng nhập</a>
                                            </li>
                                            <li>
                                                <a href="#" class="s-mymenu" style="display: none">
                                                    <span class="s-svg s-ico-login"></span><span class="s-mymenu-name"></span>
                                                    <span class="s-svg s-arw-right"></span>
                                                </a>
                                                <div class="drilldown-sub">
                                                    <div class="drilldown-back">
                                                        <a href="#" class="s-btn-back"><span class="s-svg s-arw-left"></span><span class="blind">Quay lại Menu Chiều sâu 1</span></a>
                                                    </div>
                                                    <ul>
                                                        <li class="s-depth2-wrap">
                                                            <a href="#" class="s-mymenu" style="display: none;">
                                                                <span class="s-svg s-ico-login"></span><span class="s-mymenu-name"></span>
                                                            </a>
                                                            <ul>
                                                                <li>
                                                                    <a href="#" class="s-depth3-link logoutBtn loginmenu" data-omni="logout">Thoát</a>
                                                                </li>
                                                                <!-- <li><a href="#" class="s-depth3-link logoutBtn loginmenu" data-omni="logout">Thoát</a></li> -->
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                        <!-- 20170608 한총 신규 마크업 분기처리 :: E -->
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="s-svg s-ico-close js-mo-close"><span class="blind">Close the Menu</span></button>
                            <div class="gb-gnb__sub">
                                <ul class="gb-gnb__sub-nav" role="tablist">
                                    <li class="s-depth1-wrap" role="presentation">
                                        <a href="#" class="s-btn-toggler js-toggler js-depth1-link" data-link-info="false^^" role="tab" data-omni="mobile" aria-selected="false">LIÊN HỆ</a>
                                        <div class="gb-gnb__drop-desktop" style="display: none; transition-property: opacity; transition-duration: 1ms; transition-timing-function: ease-in-out; opacity: 0;">
                                            <div class="gb-gnb__drop-inner" role="tabpanel">
                                                <div class="s-depth2-col">
                                                    <ul>
                                                        <li class="s-depth2-wrap">
                                                            <a href="business/display-solutions/magicinfo-solution/index.html" class="s-depth2-link">SỨ MỆNH<span class="s-svg "></span></a>
                                                            <div class="s-depth3-wrap">
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="s-depth2-col">
                                                    <ul>
                                                        <li class="s-depth2-wrap">
                                                            <a href="business/display-solutions/magicinfo-solution/index.html" class="s-depth2-link">TẦM NHÌN<span class="s-svg "></span></a>
                                                            <div class="s-depth3-wrap">
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="s-depth2-col s-col-featured" style="height: 268px;">
                                                    <ul>
                                                        <li class="s-depth2-wrap">
                                                            <a href="business/display-solutions/magicinfo-solution/index.html" class="s-depth2-link">GIÁ TRỊ CỐT LÕI<span class="s-svg "></span></a>
                                                            <div class="s-depth3-wrap">
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="s-depth2-col s-col-featured" style="height: 268px;">
                                                    <ul>
                                                        <li class="s-depth2-wrap">
                                                            <a href="business/display-solutions/magicinfo-solution/index.html" class="s-depth2-link">GIỚI THIỆU CÔNG TY<span class="s-svg "></span></a>
                                                            <div class="s-depth3-wrap">
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <button type="button" title="Đóng Menu" class="s-svg s-ico-close"><span class="blind">Đóng Menu</span></button>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="s-depth1-wrap">
                                        <a href="aboutsamsung/business-area/consumer-electronics/index.html" target="_blank" class="s-btn-link" data-omni="business" title="DOANH NGHIỆP : Mở trong một Cửa sổ Mới">DỊCH VỤ<span class="s-svg s-arw-right"></span></a>
                                        <div class="gb-gnb__drop-desktop" style="display: none; transition-property: opacity; transition-duration: 1ms; transition-timing-function: ease-in-out; opacity: 0;">
                                            <div class="gb-gnb__drop-inner" role="tabpanel">
                                                <div class="s-depth2-col s-col-featured" style="height: 268px;">
                                                    <ul>
                                                        <li class="s-depth2-wrap">
                                                            <a href="business/display-solutions/magicinfo-solution/index.html" class="s-depth2-link">Nhân Sự<span class="s-svg "></span></a>
                                                            <div class="s-depth3-wrap">
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="s-depth2-col s-col-featured" style="height: 268px;">
                                                    <ul>
                                                        <li class="s-depth2-wrap">
                                                            <a href="business/display-solutions/magicinfo-solution/index.html" class="s-depth2-link">Khách Hàng<span class="s-svg "></span></a>
                                                            <div class="s-depth3-wrap">
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <button type="button" title="Đóng Menu" class="s-svg s-ico-close"><span class="blind">Đóng Menu</span></button>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="s-depth1-wrap">
                                        <a href="doitac/index.html" target="_blank" class="s-btn-link" data-omni="business" title="DOANH NGHIỆP : Mở trong một Cửa sổ Mới">ĐỐI TÁC<span class="s-svg s-arw-right"></span></a>
                                        <div class="custom-layer-content" style="display:none">
                                        </div>
                                    </li>
                                    <li class="s-depth1-wrap">
                                        <a href="aboutsamsung/home/index.html" target="_blank" class="s-btn-link" data-omni="business" title="DOANH NGHIỆP : Mở trong một Cửa sổ Mới">GALLERY<span class="s-svg s-arw-right"></span></a>
                                        <div class="custom-layer-content" style="display:none">
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>
                
            </div>
    </nav>
</header>

<?php } ?>