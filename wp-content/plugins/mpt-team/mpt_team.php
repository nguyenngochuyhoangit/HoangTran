<?php
/*
Plugin Name: MPT Team
Plugin URI: http://multipurposethemes.com
Description: Declares a plugin that will create a custom post type displaying team.
Version: 1.0
Author: Multipurposethemes
Author URI: http://multipurposethemes.com
License: Custom License
*/

add_action('init', 'mpt_team', 0);

function mpt_team()
{
	$labels = array(
		'name' => _x('Team', 'Post Type General Name', 'mpt') ,
		'singular_name' => _x('Team', 'Post Type Singular Name', 'mpt') ,
		'menu_name' => __('Team', 'mpt') ,
		'name_admin_bar' => __('Team', 'mpt') ,
		'archives' => __('Item Archives', 'mpt') ,
		'parent_item_colon' => __('Parent Item:', 'mpt') ,
		'all_items' => __('All Items', 'mpt') ,
		'add_new_item' => __('Add New Item', 'mpt') ,
		'add_new' => __('Add New', 'mpt') ,
		'new_item' => __('New Item', 'mpt') ,
		'edit_item' => __('Edit Item', 'mpt') ,
		'update_item' => __('Update Item', 'mpt') ,
		'view_item' => __('View Item', 'mpt') ,
		'search_items' => __('Search Item', 'mpt') ,
		'not_found' => __('Not found', 'mpt') ,
		'not_found_in_trash' => __('Not found in Trash', 'mpt') ,
		'featured_image' => __('Featured Image', 'mpt') ,
		'set_featured_image' => __('Set featured image', 'mpt') ,
		'remove_featured_image' => __('Remove featured image', 'mpt') ,
		'use_featured_image' => __('Use as featured image', 'mpt') ,
		'insert_into_item' => __('Insert into item', 'mpt') ,
		'uploaded_to_this_item' => __('Uploaded to this item', 'mpt') ,
		'items_list' => __('Items list', 'mpt') ,
		'items_list_navigation' => __('Items list navigation', 'mpt') ,
		'filter_items_list' => __('Filter items list', 'mpt') ,
	);
	$args = array(
		'label' => __('Team', 'mpt') ,
		'description' => __('Team information page.', 'mpt') ,
		'labels' => $labels,
		'supports' => array(
			'title',
			'editor',
			'excerpt',
			'author',
			'thumbnail',
			'comments',
			'trackbacks',
			'revisions',
			'custom-fields',
			'page-attributes',
			'post-formats',
		) ,
		'taxonomies' => array(
			'team-category'
		) ,
		'hierarchical' => false,
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'menu_icon' => 'dashicons-businessman',
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'capability_type' => 'page',
	);
	register_post_type('team', $args);
}

// Register Metabox for Team

class Team_Meta_Box

{
	private $screens = array(
		'team',
	);
	private $fields = array(
		array(
			'id' => 'email-id',
			'label' => 'Email ID',
			'type' => 'text',
		) ,
		array(
			'id' => 'contact-no',
			'label' => 'Contact No',
			'type' => 'text',
		) ,
		array(
			'id' => 'position',
			'label' => 'Position',
			'type' => 'text',
		) ,
		array(
			'id' => 'company',
			'label' => 'Company',
			'type' => 'text',
		) ,	
	);
	/**
	 * Class construct method. Adds actions to their respective WordPress hooks.
	 */
	public

	function __construct()
	{
		add_action('add_meta_boxes', array(
			$this,
			'add_meta_boxes'
		));
		add_action('save_post', array(
			$this,
			'save_post'
		));
	}

	/**
	 * Hooks into WordPress' add_meta_boxes function.
	 * Goes through screens (post types) and adds the meta box.
	 */
	public

	function add_meta_boxes()
	{
		foreach($this->screens as $screen) {
			add_meta_box('team-box', __('Team Box', 'mpt') , array(
				$this,
				'add_meta_box_callback'
			) , $screen, 'side', 'default');
		}
	}

	/**
	 * Generates the HTML for the meta box
	 *
	 * @param object $post WordPress post object
	 */
	public

	function add_meta_box_callback($post)
	{
		wp_nonce_field('team_box_data', 'team_box_nonce');
		$this->generate_fields($post);
	}

	/**
	 * Generates the field's HTML for the meta box.
	 */
	public

	function generate_fields($post)
	{
		$output = '';
		foreach($this->fields as $field) {
			$label = '<label for="' . $field['id'] . '">' . $field['label'] . '</label>';
			$db_value = get_post_meta($post->ID, 'team_box_' . $field['id'], true);
			switch ($field['type']) {
			default:
				$input = sprintf('<input id="%s" name="%s" type="%s" value="%s">', $field['id'], $field['id'], $field['type'], $db_value);
			}

			$output.= '<p>' . $label . '<br />' . $input . '</p>';
		}

		echo $output;
	}

	/**
	 * Hooks into WordPress' save_post function
	 */
	public

	function save_post($post_id)
	{
		if (!isset($_POST['team_box_nonce'])) return $post_id;
		$nonce = $_POST['team_box_nonce'];
		if (!wp_verify_nonce($nonce, 'team_box_data')) return $post_id;
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return $post_id;
		foreach($this->fields as $field) {
			if (isset($_POST[$field['id']])) {
				switch ($field['type']) {
				case 'email':
					$_POST[$field['id']] = sanitize_email($_POST[$field['id']]);
					break;

				case 'text':
					$_POST[$field['id']] = sanitize_text_field($_POST[$field['id']]);
					break;
				}

				update_post_meta($post_id, 'team_box_' . $field['id'], $_POST[$field['id']]);
			}
			else
			if ($field['type'] === 'checkbox') {
				update_post_meta($post_id, 'team_box_' . $field['id'], '0');
			}
		}
	}
}

new Team_Meta_Box;

// Register Custom Taxonomy For Team

function mpt_team_taxonomy()
{
	$labels = array(
		'name' => _x('Team Categories', 'Taxonomy General Name', 'mpt') ,
		'singular_name' => _x('Team Category', 'Taxonomy Singular Name', 'mpt') ,
		'menu_name' => __('Team Category', 'mpt') ,
		'all_items' => __('All Team Categories', 'mpt') ,
		'parent_item' => __('Parent Category', 'mpt') ,
		'parent_item_colon' => __('Parent Category:', 'mpt') ,
		'new_item_name' => __('New CategoryName', 'mpt') ,
		'add_new_item' => __('Add New Category', 'mpt') ,
		'edit_item' => __('Edit Category', 'mpt') ,
		'update_item' => __('Update Category', 'mpt') ,
		'view_item' => __('View Item', 'mpt') ,
		'separate_items_with_commas' => __('Separate Categories with commas', 'mpt') ,
		'add_or_remove_items' => __('Add or remove Categories', 'mpt') ,
		'choose_from_most_used' => __('Choose from the most used categories', 'mpt') ,
		'popular_items' => __('Popular Items', 'mpt') ,
		'search_items' => __('Search Categories', 'mpt') ,
		'not_found' => __('Not Found', 'mpt') ,
		'no_terms' => __('No items', 'mpt') ,
		'items_list' => __('Items list', 'mpt') ,
		'items_list_navigation' => __('Items list navigation', 'mpt') ,
	);
	$args = array(
		'labels' => $labels,
		'hierarchical' => true,
		'public' => true,
		'show_ui' => true,
		'show_admin_column' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => true,
	);
	register_taxonomy('team-category', array(
		'team'
	) , $args);
}

add_action('init', 'mpt_team_taxonomy', 0);

add_shortcode('mpt_team', 'get_mpt_team');
function get_mpt_team( $atts ) {
    ob_start();
    $query = new WP_Query( array(
        'post_type' => 'team',
        'posts_per_page' => -1,
        'order' => 'ASC',
        'orderby' => 'menu_order',
    ) );
    if ( $query->have_posts() ) { ?>
            <?php while ( $query->have_posts() ) : $query->the_post(); ?>
			<div id="team-<?php the_ID(); ?>" class="col-md-4 col-sm-4">
				<div class="hover ehover">
					<?php echo get_the_post_thumbnail(get_the_ID(), 'full', array('class' => 'img-responsive'));?>
					<div class="overlay">
						<div class="name-block">
							<p class="h3"><?php the_title(); ?></p>
							<p><?php echo get_post_meta(get_the_ID(),'team_box_position',true)?><br/><?php echo get_post_meta(get_the_ID(),'team_box_company',true)?></p>
						</div>
						<div class="contact-details">
						<a href="mailto:<?php echo get_post_meta(get_the_ID(),'team_box_email-id',true)?>"><button class="info" data-toggle="modal" data-target="#modal4"><i class="fa fa-envelope-o"></i></button></a><a href="tel:<?php echo get_post_meta(get_the_ID(),'team_box_contact-no',true)?>"><button class="info" data-toggle="modal" data-target="#modal4"><i class="fa fa-phone"></i></button></a>
						</div>
						</div>
				</div>
			</div>
            <?php endwhile;
            wp_reset_postdata(); ?>
    <?php $myvariable = ob_get_clean();
    return $myvariable;
    }
}
?>