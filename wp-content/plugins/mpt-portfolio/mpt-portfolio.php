<?php
/*
Plugin Name: MPT Portfolio
Plugin URI: http://multipurposethemes.com
Description: Declares a plugin that will create a custom post type displaying testimonials.
Version: 1.0
Author: Multipurposethemes
Author URI: http://multipurposethemes.com
License: Custom License
*/

add_action('init', 'portfolio', 0);

function portfolio() {

    $labels = array(
        'name'                  => _x( 'Portfolio', 'Post Type General Name', 'mpt' ),
        'singular_name'         => _x( 'Portfolio', 'Post Type Singular Name', 'mpt' ),
        'menu_name'             => __( 'Portfolio', 'mpt' ),
        'name_admin_bar'        => __( 'Portfolio', 'mpt' ),
        'archives'              => __( 'Item Archives', 'mpt' ),
        'parent_item_colon'     => __( 'Parent Item:', 'mpt' ),
        'all_items'             => __( 'All Items', 'mpt' ),
        'add_new_item'          => __( 'Add New Item', 'mpt' ),
        'add_new'               => __( 'Add New', 'mpt' ),
        'new_item'              => __( 'New Item', 'mpt' ),
        'edit_item'             => __( 'Edit Item', 'mpt' ),
        'update_item'           => __( 'Update Item', 'mpt' ),
        'view_item'             => __( 'View Item', 'mpt' ),
        'search_items'          => __( 'Search Item', 'mpt' ),
        'not_found'             => __( 'Not found', 'mpt' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'mpt' ),
        'featured_image'        => __( 'Featured Image', 'mpt' ),
        'set_featured_image'    => __( 'Set featured image', 'mpt' ),
        'remove_featured_image' => __( 'Remove featured image', 'mpt' ),
        'use_featured_image'    => __( 'Use as featured image', 'mpt' ),
        'insert_into_item'      => __( 'Insert into item', 'mpt' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'mpt' ),
        'items_list'            => __( 'Items list', 'mpt' ),
        'items_list_navigation' => __( 'Items list navigation', 'mpt' ),
        'filter_items_list'     => __( 'Filter items list', 'mpt' ),
    );
    $args = array(
        'label'                 => __( 'Portfolio', 'mpt' ),
        'description'           => __( 'Portfolio information page.', 'mpt' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'page-attributes', ),
        'taxonomies'            => array( 'portfolio-category' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 6,
        'menu_icon'             => 'dashicons-portfolio',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,        
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'portfolio', $args );
}

// Register Custom Taxonomy
function portfolio_taxonomy() {

    $labels = array(
        'name'                       => _x( 'Portfolio Categories', 'Taxonomy General Name', 'mpt' ),
        'singular_name'              => _x( 'Portfolio Category', 'Taxonomy Singular Name', 'mpt' ),
        'menu_name'                  => __( 'Portfolio Category', 'mpt' ),
        'all_items'                  => __( 'All Portfolio Categories', 'mpt' ),
        'parent_item'                => __( 'Parent Category', 'mpt' ),
        'parent_item_colon'          => __( 'Parent Category:', 'mpt' ),
        'new_item_name'              => __( 'New CategoryName', 'mpt' ),
        'add_new_item'               => __( 'Add New Category', 'mpt' ),
        'edit_item'                  => __( 'Edit Category', 'mpt' ),
        'update_item'                => __( 'Update Category', 'mpt' ),
        'view_item'                  => __( 'View Item', 'mpt' ),
        'separate_items_with_commas' => __( 'Separate Categories with commas', 'mpt' ),
        'add_or_remove_items'        => __( 'Add or remove Categories', 'mpt' ),
        'choose_from_most_used'      => __( 'Choose from the most used categories', 'mpt' ),
        'popular_items'              => __( 'Popular Items', 'mpt' ),
        'search_items'               => __( 'Search Categories', 'mpt' ),
        'not_found'                  => __( 'Not Found', 'mpt' ),
        'no_terms'                   => __( 'No items', 'mpt' ),
        'items_list'                 => __( 'Items list', 'mpt' ),
        'items_list_navigation'      => __( 'Items list navigation', 'mpt' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'portfolio-category', array( 'portfolio' ), $args );

}
add_action( 'init', 'portfolio_taxonomy', 0 );


add_shortcode('mpt_portfolio', 'get_mpt_portfolio');
function get_mpt_portfolio( $atts ) {
    ob_start();
    $query = new WP_Query( array(
        'post_type' => 'portfolio',
        'posts_per_page' => -1,
        'order' => 'ASC',
        'orderby' => 'menu_order',
    ) );
	$i=0;
    if ( $query->have_posts() ) { ?>
            <?php while ( $query->have_posts() ) : $query->the_post(); 
				$active = ($i==0)?'active':'';
			?>
			<div class="item <?php echo $active;?>">
				<div class="profile-circle"><?php echo get_the_post_thumbnail(get_the_ID(), 'full', array('class' => 'img-responsive'));?></div>
				<blockquote>
					<h6 class="person fs-16"><?php echo get_post_meta(get_the_ID(),'portfolio_box_person-name',true)?></h6>
					<p class="per-den"><?php echo get_post_meta(get_the_ID(),'portfolio_box_position',true)?></p>
					<!--<p class="per-den"><?php echo get_post_meta(get_the_ID(),'portfolio_box_company',true)?></p>-->
					<p class="fs-18 quate-text"><?php the_content();?></p>
				</blockquote>
			</div>
            <?php 
			$i++;
			endwhile;
            wp_reset_postdata(); ?>
    <?php $myvariable = ob_get_clean();
    return $myvariable;
    }
}
?>