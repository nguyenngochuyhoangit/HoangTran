<?php
/*
Plugin Name: MPT Testimonials
Plugin URI: http://multipurposethemes.com
Description: Declares a plugin that will create a custom post type displaying testimonials.
Version: 1.0
Author: Multipurposethemes
Author URI: http://multipurposethemes.com
License: Custom License
*/

add_action('init', 'mpt_testimonial', 0);

function mpt_testimonial()
{
	$labels = array(
		'name' => _x('Testimonial', 'Post Type General Name', 'mpt') ,
		'singular_name' => _x('Testimonial', 'Post Type Singular Name', 'mpt') ,
		'menu_name' => __('Testimonial', 'mpt') ,
		'name_admin_bar' => __('Testimonial', 'mpt') ,
		'archives' => __('Item Archives', 'mpt') ,
		'parent_item_colon' => __('Parent Item:', 'mpt') ,
		'all_items' => __('All Items', 'mpt') ,
		'add_new_item' => __('Add New Item', 'mpt') ,
		'add_new' => __('Add New', 'mpt') ,
		'new_item' => __('New Item', 'mpt') ,
		'edit_item' => __('Edit Item', 'mpt') ,
		'update_item' => __('Update Item', 'mpt') ,
		'view_item' => __('View Item', 'mpt') ,
		'search_items' => __('Search Item', 'mpt') ,
		'not_found' => __('Not found', 'mpt') ,
		'not_found_in_trash' => __('Not found in Trash', 'mpt') ,
		'featured_image' => __('Featured Image', 'mpt') ,
		'set_featured_image' => __('Set featured image', 'mpt') ,
		'remove_featured_image' => __('Remove featured image', 'mpt') ,
		'use_featured_image' => __('Use as featured image', 'mpt') ,
		'insert_into_item' => __('Insert into item', 'mpt') ,
		'uploaded_to_this_item' => __('Uploaded to this item', 'mpt') ,
		'items_list' => __('Items list', 'mpt') ,
		'items_list_navigation' => __('Items list navigation', 'mpt') ,
		'filter_items_list' => __('Filter items list', 'mpt') ,
	);
	$args = array(
		'label' => __('Testimonial', 'mpt') ,
		'description' => __('Testimonial information page.', 'mpt') ,
		'labels' => $labels,
		'supports' => array(
			'title',
			'editor',
			'excerpt',
			'author',
			'thumbnail',
			'comments',
			'trackbacks',
			'revisions',
			'page-attributes',
		) ,
		'taxonomies' => array(
			'testimonial-category'
		) ,
		'hierarchical' => false,
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'menu_icon' => 'dashicons-megaphone',
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'capability_type' => 'page',
	);
	register_post_type('testimonial', $args);
}

// Register Metabox for Testimonial

class Testimonial_Meta_Box

{
	private $screens = array(
		'testimonial',
	);
	private $fields = array(
		array(
			'id' => 'person-name',
			'label' => 'Person Name',
			'type' => 'text',
		) ,
		array(
			'id' => 'position',
			'label' => 'Position',
			'type' => 'text',
		) ,
		/*array(
			'id' => 'company',
			'label' => 'Company',
			'type' => 'text',
		) ,	*/
	);
	/**
	 * Class construct method. Adds actions to their respective WordPress hooks.
	 */
	public

	function __construct()
	{
		add_action('add_meta_boxes', array(
			$this,
			'add_meta_boxes'
		));
		add_action('save_post', array(
			$this,
			'save_post'
		));
	}

	/**
	 * Hooks into WordPress' add_meta_boxes function.
	 * Goes through screens (post types) and adds the meta box.
	 */
	public

	function add_meta_boxes()
	{
		foreach($this->screens as $screen) {
			add_meta_box('testimonial-box', __('Testimonial Box', 'mpt') , array(
				$this,
				'add_meta_box_callback'
			) , $screen, 'side', 'default');
		}
	}

	/**
	 * Generates the HTML for the meta box
	 *
	 * @param object $post WordPress post object
	 */
	public

	function add_meta_box_callback($post)
	{
		wp_nonce_field('testimonial_box_data', 'testimonial_box_nonce');
		$this->generate_fields($post);
	}

	/**
	 * Generates the field's HTML for the meta box.
	 */
	public

	function generate_fields($post)
	{
		$output = '';
		foreach($this->fields as $field) {
			$label = '<label for="' . $field['id'] . '">' . $field['label'] . '</label>';
			$db_value = get_post_meta($post->ID, 'testimonial_box_' . $field['id'], true);
			switch ($field['type']) {
			default:
				$input = sprintf('<input id="%s" name="%s" type="%s" value="%s">', $field['id'], $field['id'], $field['type'], $db_value);
			}

			$output.= '<p>' . $label . '<br />' . $input . '</p>';
		}

		echo $output;
	}

	/**
	 * Hooks into WordPress' save_post function
	 */
	public

	function save_post($post_id)
	{
		if (!isset($_POST['testimonial_box_nonce'])) return $post_id;
		$nonce = $_POST['testimonial_box_nonce'];
		if (!wp_verify_nonce($nonce, 'testimonial_box_data')) return $post_id;
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return $post_id;
		foreach($this->fields as $field) {
			if (isset($_POST[$field['id']])) {
				switch ($field['type']) {
				case 'email':
					$_POST[$field['id']] = sanitize_email($_POST[$field['id']]);
					break;

				case 'text':
					$_POST[$field['id']] = sanitize_text_field($_POST[$field['id']]);
					break;
				}

				update_post_meta($post_id, 'testimonial_box_' . $field['id'], $_POST[$field['id']]);
			}
			else
			if ($field['type'] === 'checkbox') {
				update_post_meta($post_id, 'testimonial_box_' . $field['id'], '0');
			}
		}
	}
}

new Testimonial_Meta_Box;

// Register Custom Taxonomy For Testimonial

function testimonial_taxonomy()
{
	$labels = array(
		'name' => _x('Testimonial Categories', 'Taxonomy General Name', 'mpt') ,
		'singular_name' => _x('Testimonial Category', 'Taxonomy Singular Name', 'mpt') ,
		'menu_name' => __('Testimonial Category', 'mpt') ,
		'all_items' => __('All Testimonial Categories', 'mpt') ,
		'parent_item' => __('Parent Category', 'mpt') ,
		'parent_item_colon' => __('Parent Category:', 'mpt') ,
		'new_item_name' => __('New CategoryName', 'mpt') ,
		'add_new_item' => __('Add New Category', 'mpt') ,
		'edit_item' => __('Edit Category', 'mpt') ,
		'update_item' => __('Update Category', 'mpt') ,
		'view_item' => __('View Item', 'mpt') ,
		'separate_items_with_commas' => __('Separate Categories with commas', 'mpt') ,
		'add_or_remove_items' => __('Add or remove Categories', 'mpt') ,
		'choose_from_most_used' => __('Choose from the most used categories', 'mpt') ,
		'popular_items' => __('Popular Items', 'mpt') ,
		'search_items' => __('Search Categories', 'mpt') ,
		'not_found' => __('Not Found', 'mpt') ,
		'no_terms' => __('No items', 'mpt') ,
		'items_list' => __('Items list', 'mpt') ,
		'items_list_navigation' => __('Items list navigation', 'mpt') ,
	);
	$args = array(
		'labels' => $labels,
		'hierarchical' => true,
		'public' => true,
		'show_ui' => true,
		'show_admin_column' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud' => true,
	);
	register_taxonomy('testimonial-category', array(
		'testimonial'
	) , $args);
}

add_action('init', 'testimonial_taxonomy', 0);

add_shortcode('mpt_testimonial', 'get_mpt_testimonial');
function get_mpt_testimonial( $atts ) {
    ob_start();
    $query = new WP_Query( array(
        'post_type' => 'testimonial',
        'posts_per_page' => -1,
        'order' => 'ASC',
        'orderby' => 'menu_order',
    ) );
	$i=0;
    if ( $query->have_posts() ) { ?>
            <?php while ( $query->have_posts() ) : $query->the_post(); 
				$active = ($i==0)?'active':'';
			?>
			<div class="item <?php echo $active;?>">
				<div class="profile-circle"><?php echo get_the_post_thumbnail(get_the_ID(), 'full', array('class' => 'img-responsive'));?></div>
				<blockquote>
					<h6 class="person fs-16"><?php echo get_post_meta(get_the_ID(),'testimonial_box_person-name',true)?></h6>
					<p class="per-den"><?php echo get_post_meta(get_the_ID(),'testimonial_box_position',true)?></p>
					<!--<p class="per-den"><?php echo get_post_meta(get_the_ID(),'testimonial_box_company',true)?></p>-->
					<p class="fs-18 quate-text"><?php the_content();?></p>
				</blockquote>
			</div>
            <?php 
			$i++;
			endwhile;
            wp_reset_postdata(); ?>
    <?php $myvariable = ob_get_clean();
    return $myvariable;
    }
}
?>