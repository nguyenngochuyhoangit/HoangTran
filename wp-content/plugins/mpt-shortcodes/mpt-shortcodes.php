<?php
/*
Plugin Name: MPT Shortcodes 
Plugin URI: http://multipurposethemes.com
Description: Declares a plugin that will contain shortcodes which are used in theme.
Version: 1.0
Author: Multipurposethemes
Author URI: http://multipurposethemes.com
License: Custom License
*/

add_shortcode('corporate_getblog', 'get_corporate_blog');
function get_corporate_blog( $atts ) {
    ob_start();
    $query = new WP_Query( array(
        'post_type' => 'post',
        'posts_per_page' => 2,
        'order' => 'DESC',
    ) );
    if ( $query->have_posts() ) { 
	$i=0;
	?>
            <?php 
				while ( $query->have_posts() ) : $query->the_post(); 
				$left_right = ($i==0)?'img-left':'img-right';
				$hide_show = ($i==0)?'showme':'hideme';
				$hide_show = ($i==1)?'hideme':'showme';
			?>
			<article id="blog-<?php the_ID(); ?>" class="blog-post clearfix">
				<?php if(has_post_thumbnail()):?>
				<div class="col-md-6 col-sm-6 <?php esc_attr_e($left_right,'corporate');?>">
				<?php echo get_the_post_thumbnail(get_the_ID(), 'full', array('class' => 'img-responsive'));?>
				</div>
				<?php endif; ?>
				<div class="<?php if(has_post_thumbnail()){?>col-md-6 col-sm-6<?php }else{ ?>col-md-12 col-sm-12<?php } ?>">
					<div class="blog-content">
						<div class="date">
							<i class="fa fa-calendar"></i><?php the_time('F j, Y'); ?>
						</div>
						<div class="author">
							<i class="fa fa-folder"></i><?php echo get_the_category_list(); ?><i class="fa fa-user"></i><?php the_author(); ?>
						</div>
						<div class="blog-title">
							<h3><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h3>
						</div>
						<div class="blog-text">
						<?php the_excerpt(); ?>
						</div>
						<div class="read-btn"><a class="btn" href="<?php the_permalink();?>"> Continue Reading <i class="fa fa-arrow-right"></i></a></div>
					</div>
				</div>
			</article>
            <?php 
			$i++;
			endwhile;
            wp_reset_postdata(); ?>
    <?php $bloglist = ob_get_clean();
    return $bloglist;
    }
}

/**
 * Counter Short Code
 *
 */
function counter_shortcode( $atts, $content = null ) {    
?>
<script>
jQuery(document).ready(function($) {
jQuery(window).scroll(function(){   
jQuery('.anim_count').each(function () {
    jQuery(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 34000,
        easing: 'swing',
        step: function (now) {
            jQuery(this).text(Math.ceil(now));
        }
    });
     jQuery('.anim_count').removeClass('anim_count').addClass('anim_counted');
});
});
});
    </script>
    <?php
    return '<span class="anim_count fs-24 fw-700">' . $content . '</span>';
}
add_shortcode( 'counter', 'counter_shortcode' );


/**
 * Year shortcode
 *
 */
if ( ! function_exists( 'mpt_year_shortcode' ) ) {
	function mpt_year_shortcode() {
		return date('Y');
	}
}
add_shortcode( 'current_year', 'mpt_year_shortcode' );
?>