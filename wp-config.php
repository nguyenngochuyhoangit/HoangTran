<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'vttechvn_hoangtr');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0<SEl^s|)(Onh@2A+Olw[@c@T:jd2EM,SJ j;x7HZb]7&b3$z0B0gb/AX&csSOH8');
define('SECURE_AUTH_KEY',  'X#LBe`du;a|P*Co~)GJ<I@ly>u/+x{kNL-Yg?f_dm[4.NMY0f[r:jPaNDU]HcI>x');
define('LOGGED_IN_KEY',    '/>7cI(X)3D~l8r/06o[ 3^AUqb![=gFR|U[wbyXLiL5yH~8Rj * GiXv<jJb5V*U');
define('NONCE_KEY',        '+|T=R6Rr5k]GeD)xEQ1nVHZYLP[sy]tw(,:sa/leSavtM!p[(g2H{Y+KG:BMl Rg');
define('AUTH_SALT',        'BO>tQEhy2~DL3oA`Y&-*Xly@d/2@1ZYI{ {4.M7>d4j_6Y1hUVni*VV|8Y8pl ~D');
define('SECURE_AUTH_SALT', 'IK4mS4[OoL@pyT4#O~^PFLQ@bPK6&n-dp.8<QQ>dJV}Z-GVA>E_IhV/e#SC!s.(?');
define('LOGGED_IN_SALT',   '~s[F?C82b~UM`9I;>9A`:,eWVf,F}X|%3(&,qhz,1GEHp/lsB-H=7%_l.:JqHXiR');
define('NONCE_SALT',       'k!L{ 58NC51GPua-[u8pR|~,qlECQFKTt>GgN~3!vk6itAd}pWb,>R`eTb--QaU!');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
